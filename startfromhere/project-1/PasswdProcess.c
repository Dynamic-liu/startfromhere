/**
 *
 *  File : PasswdProcess.c  Date: 2016-09-19
 *  ========================================
 *  Target :
 *      to identify the kind of encoding format
 *
 */

#include <stdio.h>
#include <string.h>

//declare the defualt max length
int STR_LEN=1024;


/*
 * Process a single line of the file
 * 
 * Parameter : line : Line to process from the file username :
 *   Username from the line (pass-by-reference) 
 *   hash_id : Hash method id (pass-by-reference) 
 *   hash_salt : Salt used for this hash without id (pass-by-reference)
 *   hash_pwd : The hashed password without salt or id (pass-by-reference)
 *
 * Returns :
 *   -1 if line is not a valid username / password line
 *   0 on success
 */
int process_line(char *line, char **username,int *hash_id,char **hash_salt,char **hash_pwd)
{
  char *temp;
  char judge[4]; // the judge of encoding format the 4-th position is '\0'

  //This means that there is no ':' in this string
  if(!strchr(line,':')){
    printf("Error : Input line missing the ':' seperator. Could not find username/password! \n");
    return -1;
  }

  //use the strtok to get the usernmae
  temp=strdup(line);
  *username=strtok(temp,":");
  if(strlen(*username)<=3){
    printf("Error : Username length incorrect . Expected >3 , Actural %d \n",strlen(*username));
    return -1;
  }

  //dump the left line into temp for the next manipute
  temp=strdup(&(line[strchr(line,':')-line]));
  
  //copy the first two characters to do the next judge
  strncpy(judge,temp,3);
  judge[3]='\0' ;
  *hash_id=ger_coding_format(judge);

  //dealing the hash_id result
  if(*hash_id==-1){
    printf("Error : Unknow hash ID\n");
    return -1;
  }else if(*hash_id=0){
    strncpy(*hash_salt,temp,2);
    (*hash_salt)[2]='\0';
    *hash_pwd=strdup(&temp[2]);
    if(strlen(*hash_pwd)!=11){
      printf("Error : DES password length incorrect! Except : 11, Actual %d\n",strlen(*hash_pwd));
      return -1;
    }
  }else if(*hash_id=1){
    *hash_salt=strtok(*(&temp[3]),'$'); if(strlen(*hash_salt)!=8){
      printf("Error : MDS salt length incorrect! Expect : 8, Actual %d\n",strlen(*hash_salt));
      return -1;
    }
    (*hash_salt)[8]='\0';
    *hash_pwd=strtok(NULL,'$');
    if(strlen(*hash_pwd)!=22){
      printf("Error : MDS password length incorrect! Expect : 22, Actual %d\n",strlen(*hash_pwd));
      return -1;
    }
    (*hash_pwd)[22]='\0';
  }else if(*hash_id==5){
    *hash_salt=strtok(&temp[3],'$');
    if(strlen(*hash_salt)!=16){
      printf("Error : SHA-256 salt length incorrect! Expect : 16, Actual %d\n",strlen(*hash_salt));          
	return -1; 
    }
    (*hash_salt)[16]='\0';
    *hash_pwd=strtok(NULL,'$');
    if(strlen(*hash_pwd)!=43){
      printf("Error : SHA-256 password length incorrect! Expect : 43, Actual %d\n",strlen(*hash_pwd));
      return -1;
    }
    (*hash_pwd)[43]='\0';
  }else if(*hash_id==6){
    *hash_salt=strtok(&temp[3],'$');
    if(strlen(*hash_salt)!=16){
      printf("Error : SHA-512 salt length incorrect! Expect : 16, Actual %d\n",strlen(*hash_salt));
      return -1;
    }
    (*hash_salt)[16]='\0';
    *hash_pwd=strtok(NULL,'$');
    if(strlen(*hash_pwd)!=86){
      printf("Error : SHA-512 password length incorrect! Expect : 86, Actual %d\n",strlen(*hash_pwd));
      return -1;
    }
    (*hash_pwd)[86]='\0';
  }
    
  return 0;
}
               

/**
 * judge the real encoding format
 * 
 * Paramters:
 *   judge : used to judge style
 *
 * Returns :
 *   0 : means DES which start with two character
 *   1 : means MD5 $1
 *   5 : means SHA-256 $5
 *   6 : means SHA-512 $6
 *  -1 : means not a correct encoding style
 */
int get_coding_format(char *judge)
{
  if(!judge || strlen(judge)<=3) return -1;
  else if(strcmp(judge,"$1$")) return 1;
  else if(strcmp(judge,"$5$")) return 5;
  else if(strcmp(judge,"$6$")) return 6;
  else return 0;
}

/**
 * Turn the int hash id to string 
 *
 * Parameter :
 *    id : the hash type id
 *    hash_type : the string type
 * Returns :
 *    
 */
void id_to_string(int id,char **hash_type)
{
   switch(id){
	case 0 :
	 strcpy(hash_type,"DES");
	 return;
	case 1 :
	 strcpy(hash_type,"MD5");
	 retuen;
	case 5 :
	 strcpy(hash_type,"SHA-256");
	 return;
	case 6 :
	 strcpy(hash_type,"SHA-512");
	 return;
   } 
	
}

int main()
{
   char line[]="alice:GMuMeMoqUogqE";
   char username[STR_LEN];
   char hash_salt[STR_LEN];
   char hash_pwd[STR_LEN];
   char hash_type[7];
   int  hash_id;

   printf("Process : %s\n";line);
   process_line(line,&username,&hash_id,&hash_salt,&hash_pwd);
   id_to_string(hash_id,&hash_type);

   printf("Name : %s , Hash_type : %s, Hash salt : %s , Hash password : %s\n",
	  username, hash_type , hash_salt , hash_pwd);

   return 0;
}
