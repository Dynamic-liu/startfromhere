/**********************
 *  File: StringManipute.c  Create at: 2016-09-12
 *  ====================
 *  target:
 *      manipute the standard input to output the 
 *  record of different character
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

//The default length of a line if 1024
int STR_LEN  = 1024;

/**
 * Dissect the string by counting the types of letters contained within.
 *
 * parameters :
 *    str : Input string to process         
 *    num_space : Update to reflect the number of spaces
 *    num_digit : Update to reflect the number of digits
 *    num_lower : Update to reflect the number of lower characters
 *    num_upper : Update to reflect the number of upper characters
 *    num_other : Updare to reflect the number of other characters
 *
 * retutns :
 *    Length of the string.
 */
int dissect_string(char *str, int *num_space, int *num_digit, int *num_lower
		   , int *num_upper, int *num_other)
{
  char cur;
  int len=0;
  
  //add into different kind record according the judge 
  while((cur=str[len])!='\0'){
    if(isspace(cur)) *num_space=(*num_space) + 1;
    else if(isdigit(cur)) *num_digit=(*num_digit) + 1;
    else if(islower(cur)) *num_lower=(*num_lower) + 1;
    else if(isupper(cur)) *num_upper=(*num_upper) + 1;
    else *num_other=(*num_other) + 1;

    len++;
  }
 
  return len;//remove the last '\0' char
}
  

/**
 * Print the total information about the count
 *
 * parameters :
 *    line        : the line infornation
 *    total_space : Update to reflect the number of spaces
 *    total_digit : Update to reflect the number of digits
 *    total_lower : Update to reflect the number of lower characters
 *    total_upper : Update to reflect the number of upper characters
 *    total_other : Updare to reflect the number of other characters
 *     
 * returns : void
 */
void printall(int line, int total_space, int total_digit,int total_lower,
 	      int total_upper, int total_other)
{
   printf(" #------------------------------------------\n");
   printf(" # Number of lines   : %d \n",line);
   printf(" # Number of space   : %d \n",total_space);
   printf(" # Number of digit   : %d \n",total_digit);
   printf(" # Number of lower   : %d \n",total_lower);
   printf(" # Number of upper   : %d \n",total_upper);
   printf(" # Number of other   : %d \n",total_other);
   printf(" #------------------------------------------\n");
}

/**
 *  Main method of the program
 *
 *  Parameters :
 *    input through the console
 *
 *  Returns :
 *    int means exit in a normal or wrong mode
 */
int main()
{
  int *num_space=(int *)(malloc (sizeof(int)));
  int *num_digit=(int *)(malloc (sizeof(int)));
  int *num_lower=(int *)(malloc (sizeof(int)));
  int *num_upper=(int *)(malloc (sizeof(int)));
  int *num_other=(int *)(malloc (sizeof(int)));
  char input_line[STR_LEN];

  int line=0;
  int total_space=0;
  int total_digit=0;
  int total_lower=0;
  int total_upper=0;
  int total_other=0;
  int curlen;

  while(1){
    	//initialize of the pointer value
    	*num_space=0;
	*num_digit=0;
	*num_lower=0;
	*num_upper=0;
	*num_other=0;
	curlen=0;

        printf("$$ ");

  	if(!fgets(input_line,STR_LEN,stdin)){
		printf("\n");
		break;
	}
	
	if('\n' == input_line[strlen(input_line)-1]){
		input_line[strlen(input_line)-1]='\0';
	}

	if((strcmp(input_line,"quit"))==0 ){
		break;
	}

	line++;
     	
	printf("  %d) \"%s\"\n",line,input_line);
	
	curlen=dissect_string(input_line,num_space,num_digit,num_lower,num_upper
			,num_other);	

	printf("  %d /\t%d /\t%d /\t%d /\t%d /\t%d \n",curlen,*num_space,
		*num_digit, *num_lower, *num_upper, *num_other);
        
	total_space= total_space + (*num_space);
	total_digit= total_digit + (*num_digit);
	total_lower= total_lower + (*num_lower);
	total_upper= total_upper + (*num_upper);
	total_other= total_other + (*num_other);

        fflush(NULL);
 }

  free(num_space);
  free(num_digit);
  free(num_lower);
  free(num_upper);
  free(num_other);

  printall(line,total_space,total_digit,total_lower,total_upper,total_other);
  return 0;
} 
