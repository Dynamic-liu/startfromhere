#include <stdio.h>

/**********************************
# create by: Dynamic-liu
# create at: 2016-09-09
# Target:
#    the first project for hello world!
#**********************************
*/

int main()
{
  
  //print the helloworld!
  printf("Hello World! \n");
  return 0;
}
