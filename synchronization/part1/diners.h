/*
 * Samantha Foley
 *
 * Project 2: Dining Philosophers (Common header)
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include "semaphore_support.h"


/*****************************
 * Defines
 *****************************/
#define TRUE  1
#define FALSE 0

#define THINKING 0
#define HUNGRY 1
#define EATING 2

/* 1.0 second = 1000000 usec */
//#define TIME_TO_SLEEP 100000
/* 0.5 second = 500000 usec */
//#define TIME_TO_SLEEP 50000
#define TIME_TO_SLEEP 5000
#define  LEFT (i + NUM_DINERS-1) % NUM_DINERS
#define  RIGHT (i + 1) % NUM_DINERS
#define MAX_VALUE 1024



/*****************************
 * Structures
 *****************************/


/*****************************
 * Global Variables
 *****************************/
int NUM_DINERS;
int state[MAX_VALUE];
semaphore_t mutex;
semaphore_t chopstick[MAX_VALUE];
/*****************************
 * Function Declarations
 *****************************/
void *philosopher(void *i);

void take_chopsticks(int i);

void release_chopsticks(int i);

void test(int i);
