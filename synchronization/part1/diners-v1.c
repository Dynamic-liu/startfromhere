/*
 * Samantha Foley
 *
 * Project 2: Dining Philosophers
 * Deadlocking Algorithm
 *
 */
#include "diners.h"

int main(int argc, char * argv[]) {
  int time_to_run = 0;
  struct timeval temp1, temp2;
  long elapsed_seconds;
  gettimeofday(&temp1,NULL);
  
  if(argc==1){
    printf("Requirement arguement: time to run in seconds.\n");
    exit(0);
  }
  
  if(argc==2){
    if(atoi(argv[1])==0){
      printf("Time to run in seconds is a positive integer greater than 0.\n");
      exit(0);
    }else{
      time_to_run = atoi(argv[1]);
      NUM_DINERS = 5;
    }
  }
  
  if(argc==3){
    if(atoi(argv[1])==0){
      printf("Time to run in seconds is a positive integer greater than 0.\n");
      exit(0);
    }
    if(atoi(argv[2])==0){
      printf("Number of philosophers is a positive integer great than 0.\n");
      exit(0);
    }
    time_to_run = atoi(argv[1]);
   NUM_DINERS = atoi(argv[2]);
  }

  pthread_t threads[NUM_DINERS];
  semaphore_create(&mutex, 1);
  int i, rc;
  for(i=0;i<NUM_DINERS;i++){
    state[i]=0;
    semaphore_create(&chopstick[i],0);
  }
  for(i=0;i<NUM_DINERS;i++){
   printf("Philosopher %d: Thinking\n", i);
   rc = pthread_create(&threads[i],NULL, philosopher, (void *)(intptr_t)i);
   if (0 != rc){
     printf("ERROR; return code from pthread_create() is %d\n", rc); exit(-1);
   }
  }

  for(i=0;i<NUM_DINERS;i++){
    pthread_join(threads[i],NULL);
  }

  gettimeofday(&temp2,NULL);
  elapsed_seconds = temp2.tv_sec -temp1.tv_sec;
   while(elapsed_seconds<time_to_run)
    ;
  
  pthread_exit(NULL);
 
  return 0;
}

void *philosopher(void *num){
  srand(time(NULL));
  while(TRUE){
    int *i = num;
    usleep(rand()%TIME_TO_SLEEP);
    printf("debug 0 *i is %d\n",*i);
    take_chopsticks(*i);
    printf("debug philosopher 1\n");
    usleep(rand()%TIME_TO_SLEEP);
     printf("debug philosopher 2\n");
    release_chopsticks(*i);
     printf("debug philosopher 3\n");
  }
 
}

void take_chopsticks(int i){
  printf("take chopsticks executed\n");
  semaphore_wait(&mutex);
  state[i] = HUNGRY;
  test(i);
  semaphore_post(&mutex);
  semaphore_wait(&chopstick[i]);
  
}

void release_chopsticks(int i){
  printf("release chopstick executed\n");
  semaphore_wait(&mutex);
  state[i] = THINKING;
  printf("Philosopher %d: ...... .... Done!\n",i);
  test(LEFT);
  test(RIGHT);
  semaphore_post(&mutex);
}

void test(int i){
  if(state[i]==HUNGRY && state[LEFT]!=EATING && state[RIGHT]!=EATING){
    printf("test executed\n");
    state[i] = EATING;
    printf("Philosopher %d: ......Eat!\n",i);
    semaphore_post(&chopstick[i]);
  }

}
