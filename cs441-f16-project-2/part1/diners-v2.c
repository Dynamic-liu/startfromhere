/*
 * Jiang Ying
 *
 * Project 2: Dining Philosophers
 * Deadlock Free Algorithm
 *
 */
#include "diners.h"

int main(int argc, char * argv[]) {
  srand(time(NULL));
  gettimeofday(&temp1,NULL);
  int rtn;
  if(0!=(rtn = parse_args(argc, argv))){
    return -1;
  }

  pthread_t threads[NUM_DINERS];
  semaphore_create(&mutex, 1);

  int i, rc;
  for(i=0;i<NUM_DINERS;i++){
    state[i]=0;
    semaphore_create(&chopstick[i],0);
  }
  for(i=0;i<NUM_DINERS;i++){
    rc = pthread_create(&threads[i],NULL, philosopher,(void *)(intptr_t)i);
   if (0 != rc){
     printf("ERROR; return code from pthread_create() is %d\n", rc);
     exit(-1);
   }
  }
  
  //clean up
   for(i=0;i<NUM_DINERS;i++){
    pthread_join(threads[i],NULL);
  }

   //print the result
    printf("\n----------------------------------------\n");
    for(i=0;i<NUM_DINERS;i++){
      printf("Philospher %d Ate %d / Thought %d\n", i,total_eating[i], total_thinking[i]);
    }
    printf("\n----------------------------------------\n");
    
    pthread_exit(NULL);
  return 0;
}

/*
 * When time runs out, terimnates loop
 * thinking for a while
 * take the chopsticks
 * eating for a while
 * put down chopsticks
 *
 */


void *philosopher(void *num){
  int i = (intptr_t)num;
  while(elapsed_seconds<time_to_run){
    gettimeofday(&temp2,NULL);
    elapsed_seconds = temp2.tv_sec -temp1.tv_sec;
   
    usleep(rand()%TIME_TO_SLEEP);
    take_chopsticks(i);
    usleep(rand()%TIME_TO_SLEEP);
    release_chopsticks(i);
  }
  pthread_exit(NULL);
}

void take_chopsticks(int i){
  semaphore_wait(&mutex);
  printf("Philospher %d: Thinking!\n",i);
  state[i] = HUNGRY;
  total_thinking[i]++;
  test(i);
  semaphore_post(&mutex);
  semaphore_wait(&chopstick[i]); 
}

void release_chopsticks(int i){
  semaphore_wait(&mutex);
  state[i] = THINKING;
  printf("Philospher %d: ........ .... Done!\n",i);
  test(LEFT);
  test(RIGHT);
  semaphore_post(&mutex);
}

void test(int i){
  if(state[i]==HUNGRY && state[LEFT]!=EATING && state[RIGHT]!=EATING){
    state[i] = EATING;
    printf("Philospher %d: ........ Eat!\n",i);
    total_eating[i]++;
    semaphore_post(&chopstick[i]);
  }
}

/*
 * get the input and then parse them into time and numbers of philosophers
 *
 */
int parse_args(int argc, char *argv[]){
  if(argc==1){
    printf("Requirement arguement: time to run in seconds.\n");
    return -1;
  }
  
  if(argc==2){
    if(atoi(argv[1])==0){
      printf("Time to run in seconds is a positive integer greater than 0.\n");
      return -1;
    }else{
      time_to_run = atoi(argv[1]);
      NUM_DINERS = 5;
    }
  }
  
  if(argc==3){
    if(atoi(argv[1])==0){
      printf("Time to run in seconds is a positive integer greater than 0.\n");
      return -1;
    }
    if(atoi(argv[2])==0){
      printf("Number of philosphers is a positive integer great than 0.\n");
      return -1;
    }
   time_to_run = atoi(argv[1]);
   NUM_DINERS = atoi(argv[2]);
  }
  if(argc>3){
    printf("Too many arguements!\n");
    return -1;
  }

  return 0;
}
